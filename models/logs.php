<?php
namespace models;


class logs {

    public $LOG_ID;
    public $LOG_DATE;
    public $LOG_FUNCTION;
    public $LOG_LEVEL;
    public $LOG_MESSAGE;

    #region gettersAndSetters

    /**
     * @return mixed
     */
    public function getLOGID()
    {
        return $this->LOG_ID;
    }

    /**
     * @param mixed $LOG_ID
     * @return logs
     */
    public function setLOGID($LOG_ID)
    {
        $this->LOG_ID = $LOG_ID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLOGDATE()
    {
        return $this->LOG_DATE;
    }

    /**
     * @param mixed $LOG_DATE
     * @return logs
     */
    public function setLOGDATE($LOG_DATE)
    {
        $this->LOG_DATE = $LOG_DATE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLOGFUNCTION()
    {
        return $this->LOG_FUNCTION;
    }

    /**
     * @param mixed $LOG_FUNCTION
     * @return logs
     */
    public function setLOGFUNCTION($LOG_FUNCTION)
    {
        $this->LOG_FUNCTION = $LOG_FUNCTION;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLOGLEVEL()
    {
        return $this->LOG_LEVEL;
    }

    /**
     * @param mixed $LOG_LEVEL
     * @return logs
     */
    public function setLOGLEVEL($LOG_LEVEL)
    {
        $this->LOG_LEVEL = $LOG_LEVEL;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLOGMESSAGE()
    {
        return $this->LOG_MESSAGE;
    }
    /**
     * @param mixed $LOG_MESSAGE
     * @return logs
     */
    public function setLOGMESSAGE($LOG_MESSAGE)
    {
        $this->LOG_MESSAGE = $LOG_MESSAGE;
        return $this;
    }

#endregion


    public static function PostALog($LOG_FUNCTION, $LOG_LEVEL,$LOG_MESSAGE){
        $db = DB::getInstance();
        $query = $db->prepare('INSERT INTO logs (LOG_FUNCTION, LOG_LEVEL, LOG_MESSAGE) VALUES (:LOG_FUNCTION, :LOG_LEVEL, :LOG_MESSAGE)');
        $query->execute([
            "LOG_FUNCTION" => $LOG_FUNCTION,
            "LOG_LEVEL" => $LOG_LEVEL,
            "LOG_MESSAGE" => $LOG_MESSAGE,
        ]);
    }

    public static function GetAllLogs(){
        $db = DB::getInstance();
        $query = $db->prepare('SELECT LOG_DATE, LOG_FUNCTION, LOG_LEVEL, LOG_MESSAGE FROM logs ORDER BY LOG_ID DESC');
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_CLASS, 'models\logs');
    }

}