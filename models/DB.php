<?php
namespace models;
use PDO;

if(ROOT[0] == 'D')
    require_once('configDev.php');
else
    require_once('configProd.php');

class DB{

    private static $_instance = null;

    protected function __construct() { } // private contructor
    protected function __clone() { } // private cloning method to avoid cloning this class

    public static function initInstance()
    {
        try{
            SELF::$_instance = new PDO('mysql:host='.DBhostname.';dbname='.DBname.';charset=utf8', DBusername, DBpassword);
            SELF::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(\Exception $e){
            SELF::$_instance = 'Erreur : '.$e->getMessage();
        }
    }

    public static function getInstance()
    {
        if(SELF::$_instance == null){
            SELF::initInstance();
        }
        return SELF::$_instance;
    }
}