<?php
namespace models;

class user {
    public $USERS_ID;
    public $USERS_LOGIN;
    public $USERS_NAME;
    public $USERS_PHONE;
    public $USERS_ADDRESS;
    public $USERS_ZIP;
    public $USERS_CITY;
    public $USERS_PASSWORD;
    public $USERS_ROLE;
    public $USERS_TEMPTOKEN;
    public $USERS_TEMPTOKENVALIDITY;


    #region getters_setters

    /**
     * @return mixed
     */
    public function getUSERSID()
    {
        return $this->USERS_ID;
    }
    /**
     * @param mixed $USERS_ID
     * @return user
     */
    public function setUSERSID($USERS_ID)
    {
        $this->USERS_ID = $USERS_ID;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSLOGIN()
    {
        return $this->USERS_LOGIN;
    }

    /**
     * @param mixed $USERS_LOGIN
     * @return user
     */
    public function setUSERSLOGIN($USERS_LOGIN)
    {
        $this->USERS_LOGIN = $USERS_LOGIN;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSPHONE()
    {
        return $this->USERS_PHONE;
    }

    /**
     * @param mixed $USERS_PHONE
     * @return user
     */
    public function setUSERSPHONE($USERS_PHONE)
    {
        $this->USERS_PHONE = $USERS_PHONE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSADDRESS()
    {
        return $this->USERS_ADDRESS;
    }

    /**
     * @param mixed $USERS_ADDRESS
     * @return user
     */
    public function setUSERSADDRESS($USERS_ADDRESS)
    {
        $this->USERS_ADDRESS = $USERS_ADDRESS;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSZIP()
    {
        return $this->USERS_ZIP;
    }

    /**
     * @param mixed $USERS_ZIP
     * @return user
     */
    public function setUSERSZIP($USERS_ZIP)
    {
        $this->USERS_ZIP = $USERS_ZIP;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSCITY()
    {
        return $this->USERS_CITY;
    }

    /**
     * @param mixed $USERS_CITY
     * @return user
     */
    public function setUSERSCITY($USERS_CITY)
    {
        $this->USERS_CITY = $USERS_CITY;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSPASSWORD()
    {
        return $this->USERS_PASSWORD;
    }

    /**
     * @param mixed $USERS_PASSWORD
     * @return user
     * @throws \Exception
     */

    public function setUSERSPASSWORD($USERS_PASSWORD, $USERS_PASSWORD_CHECK)
    {
        if(strlen($USERS_PASSWORD)<=5)
            throw new \Exception("Password must be at least 5 characters long");

        if($USERS_PASSWORD == $USERS_PASSWORD_CHECK)
            $this->USERS_PASSWORD = password_hash($USERS_PASSWORD, PASSWORD_DEFAULT);
        else
            throw new \Exception("Passwords don't match");

        return $this;

    }

    /**
     * @return mixed
     */
    public function getUSERSROLE()
    {
        return $this->USERS_ROLE;
    }

    /**
     * @param mixed $USERS_ROLE
     * @return user
     */
    public function setUSERSROLE($USERS_ROLE)
    {
        $this->USERS_ROLE = $USERS_ROLE;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSNAME()
    {
        return $this->USERS_NAME;
    }

    /**
     * @param mixed $USERS_NAME
     * @return user
     */
    public function setUSERSNAME($USERS_NAME)
    {
        $this->USERS_NAME = $USERS_NAME;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSTEMPTOKEN()
    {
        return $this->USERS_TEMPTOKEN;
    }

    /**
     * @param mixed $USERS_TEMPTOKEN
     * @return user
     */
    public function setUSERSTEMPTOKEN($USERS_TEMPTOKEN)
    {
        $this->USERS_TEMPTOKEN = $USERS_TEMPTOKEN;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUSERSTEMPTOKENVALIDITY()
    {
        return $this->USERS_TEMPTOKENVALIDITY;
    }

    /**
     * @param mixed $USERS_TEMPTOKENVALIDITY
     * @return user
     */

     public function setUSERSTEMPTOKENVALIDITY($USERS_TEMPTOKENVALIDITY)
    {
    $this->USERS_TEMPTOKENVALIDITY = $USERS_TEMPTOKENVALIDITY;
    return $this;
    }

#endregion

    public function CheckUserLoginAndPassword($loginFromForm, $passwordFromForm){
        try{
            $error = "";
            $success = false;
            if($this->_CheckUserLogin($loginFromForm)){
                if($this->_CheckUserPassword($passwordFromForm)){
                    $tempToken = $this->_AttributeNewTempTokenToUser();
                    $success = true;
                    logs::PostALog("user.CheckUserLoginAndPassword", "information", $loginFromForm." is logged");
                }
                else{
                    $error =  "There is something odd with the password...";
                }
            }
            else{
                $error = "Are you sure, this is your login ?";
            }
        }
        catch(\Exception $ex){
            logs::PostALog('user.CheckUserLoginAndPassword', 'error', $ex->getMessage());
            $error = 'Hu-ho, something went wrong, please contact the website administrator';
        }
        finally{
            return [
                'success' => $success,
                'temptoken' => $tempToken?? "",
                'role' => $this->getUSERSROLE()??"",
                'username' => $this->getUSERSPASSWORD(),
                'error' => $error
            ];
        }

    }

    public function IsTokenStillValid(){
        try{
            $db = DB::getInstance();
            $token = $this->getUSERSTEMPTOKEN();
            if($token != ""){
                $query = $db->prepare("SELECT SYSDATE() AS 'CURRENT_DATE', USERS_TEMPTOKENVALIDITY FROM users WHERE USERS_TEMPTOKEN =:tempToken");
                $query->execute([
                    "tempToken"=> $token
                ]);

                $result = $query->fetch();
                if(!$result)
                    throw new \Exception("Query SELECT SYSDATE() AS 'CURRENT_DATE', USERS_TEMPTOKENVALIDITY FROM users WHERE USERS_TEMPTOKEN = '${$token}' returned false");

                if (($result["USERS_TEMPTOKENVALIDITY"] >= $result["CURRENT_DATE"]) && $result["USERS_TEMPTOKENVALIDITY"] != null)
                    return true;
            }
        }
        catch(\Exception $ex){
            logs::PostALog('user._AttributeNewTempTokenToUser', 'error', $ex->getMessage());
        }
        return false;
    }

    public function GetAllInfoForOneUserFromTempToken(){
         try{
             $db = DB::getInstance();
             $query = $db->prepare("SELECT USERS_ID, USERS_LOGIN, USERS_ROLE, USERS_NAME, USERS_PHONE, USERS_ADDRESS, USERS_ZIP, USERS_CITY, USERS_TEMPTOKEN FROM users WHERE USERS_TEMPTOKEN=:tempToken");
             $query->setFetchMode(\PDO::FETCH_CLASS, 'models\user');
             $query->execute([
                 "tempToken" => $this->getUSERSTEMPTOKEN()
             ]);
             return  $query->fetch();
         }
         catch(\Exception $e){
             logs::PostALog('user.GetAllInfoForOneUserFromTempToken', 'error', $e->getMessage());
             throw $e;
         }
    }

    public function GetSafeUserInfoForOneUserFromTempToken(){
        try{
            $db = DB::getInstance();
            $query = $db->prepare("SELECT USERS_LOGIN, USERS_NAME, USERS_PHONE, USERS_ADDRESS, USERS_ZIP, USERS_CITY FROM users WHERE USERS_TEMPTOKEN=:tempToken");
            $query->setFetchMode(\PDO::FETCH_CLASS, 'models\user');
            $query->execute([
                "tempToken" => $this->getUSERSTEMPTOKEN()
            ]);
            return  $query->fetch();
        }
        catch(\Exception $e){
            logs::PostALog('user.GetSafeUserInfoForOneUserFromTempToken', 'error', $e->getMessage());
            throw $e;
        }
    }

    public function RegisterOneUser(){

        try{
            $db = DB::getInstance();
            $query = $db->prepare("INSERT INTO users (USERS_LOGIN, USERS_PASSWORD, USERS_NAME, USERS_PHONE, USERS_ADDRESS, USERS_ZIP, USERS_CITY) VALUES (:USERS_LOGIN, :USERS_PASSWORD, :USERS_NAME, :USERS_PHONE, :USERS_ADDRESS, :USERS_ZIP, :USERS_CITY)");
            return $query->execute([
                'USERS_LOGIN'=>$this->getUSERSLOGIN(),
                'USERS_PASSWORD'=>$this->getUSERSPASSWORD(),
                'USERS_NAME'=>$this->getUSERSNAME(),
                'USERS_PHONE'=>$this->getUSERSPHONE(),
                'USERS_ADDRESS'=>$this->getUSERSADDRESS(),
                'USERS_ZIP'=>$this->getUSERSZIP(),
                'USERS_CITY'=>$this->getUSERSCITY()
            ]);
        }
        catch(\Exception $e){
            logs::PostALog('user.GetAllInfoForOneUserFromTempToken', 'error', $e->getMessage());
            return false;
        }

    }

    public function UpdateAllUserInfoFromUserAccount(){
        try{
            $db = DB::getInstance();
            $query = $db->prepare("UPDATE users SET USERS_NAME=:USERS_NAME, USERS_PHONE=:USERS_PHONE, USERS_ADDRESS=:USERS_ADDRESS, USERS_ZIP=:USERS_ZIP, USERS_CITY=:USERS_CITY WHERE USERS_TEMPTOKEN=:USERS_TEMPTOKEN");
            return $query->execute([
                'USERS_NAME'=>$this->getUSERSNAME(),
                'USERS_PHONE'=>$this->getUSERSPHONE(),
                'USERS_ADDRESS'=>$this->getUSERSADDRESS(),
                'USERS_ZIP'=>$this->getUSERSZIP(),
                'USERS_CITY'=>$this->getUSERSCITY(),
                'USERS_TEMPTOKEN'=>$this->getUSERSTEMPTOKEN(),
            ]);
        }
        catch(\Exception $e){
            logs::PostALog('user.UpdateUserInfoWithUserAccount', 'error', $e->getMessage());
            return false;
        }

    }

    public function UpdateUserInfoFromAdminAccount(){
        try{
            $db = DB::getInstance();
            $query = $db->prepare("UPDATE users SET USERS_ROLE=:USERS_ROLE WHERE USERS_LOGIN=:USERS_LOGIN");
            return $query->execute([
                'USERS_LOGIN'=>$this->getUSERSLOGIN(),
                'USERS_ROLE'=>$this->getUSERSROLE()
            ]);
        }
        catch(\Exception $e){
            logs::PostALog('user.UpdateUserInfoFromAdminAccount', 'error', $e->getMessage());
            return false;
        }
    }

    public function UpdateUserPassword($passwordCurrent, $passwordNew, $passwordNewCheck){
        try{
            $db = DB::getInstance();
            if($this->_CheckUserPassword($passwordCurrent)){
                $this->setUSERSPASSWORD($passwordNew, $passwordNewCheck);

                $query = $db->prepare("UPDATE users SET USERS_PASSWORD=:USERS_PASSWORD WHERE USERS_TEMPTOKEN=:USERS_TEMPTOKEN");
                return $query->execute([
                    'USERS_PASSWORD'=>$this->getUSERSPASSWORD(),
                    'USERS_TEMPTOKEN'=>$this->getUSERSTEMPTOKEN()
                ]);
            }
            else{
                throw new \Exception('Please recheck your current password');
            }

        }
        catch(\Exception $e){
            logs::PostALog('user.UpdateUserPassword', 'error', $e->getMessage());
            throw $e;
        }
    }

    public function GetAdminInfoOfAllUsers(){
        try{
            $db = DB::getInstance();
            $query = $db->prepare("SELECT USERS_LOGIN, USERS_ROLE FROM users");
            $query->execute();
            return $query->fetchAll(\PDO::FETCH_CLASS, 'models\user' );
        }
        catch(\Exception $e){
            logs::PostALog('user.GetAdminInfoOfAllUsers', 'error', $e->getMessage());
            return false;
        }
    }

    public function RemoveUser(){
        try{
            $db = DB::getInstance();
            $query = $db->prepare("DELETE FROM users WHERE USERS_LOGIN=:USERS_LOGIN");
            $query->execute([
                'USERS_LOGIN'=>$this->getUSERSLOGIN()
            ]);
        }
        catch(\Exception $e){
            logs::PostALog('user.RemoveUser', 'error', $e->getMessage());
            return false;
        }
    }

    public static function IsThisUserLoginAvailable($loginFromForm){
        try{
            $db = DB::getInstance();
            $query = $db->prepare('SELECT USERS_LOGIN FROM users WHERE USERS_LOGIN = :login');
            $query->execute([
                "login" => $loginFromForm
            ]);
            $result = $query->fetch();
            if(count($result) > 1)
                return false;
            return true;
        }
        catch(\Exception $ex){
            logs::PostALog('user._IsThisUserNameTaken', 'error', $ex->getMessage());
        }
    }

    private function _CheckUserLogin($loginFromForm){
        try{
            $db = DB::getInstance();
            $query = $db->prepare('SELECT USERS_ID, USERS_LOGIN, USERS_ROLE, USERS_NAME FROM users WHERE USERS_LOGIN = :login');
            $query->execute([
                "login" => $loginFromForm
            ]);
            $result = $query->fetchAll();
            if(count($result) > 1)
                throw new \Exception("More than one user found within the login ${$loginFromForm}");
            $loginReference = $result[0]["USERS_LOGIN"]?? "";
            $userId = $result[0]["USERS_ID"]?? "";
            $userRole = $result[0]["USERS_ROLE"]?? "";
            $userName = $result[0]["USERS_NAME"]?? "";
            if($loginFromForm == $loginReference){
                $this->setUSERSLOGIN($loginFromForm);
                $this->setUSERSID($userId);
                $this->setUSERSROLE($userRole);
                $this->setUSERSNAME($userName);
            }

            return $loginFromForm == $loginReference;
        }
        catch(\Exception $ex){
            logs::PostALog('user._CheckUserLogin', 'error', $ex->getMessage());
        }
    }

    private function _CheckUserPassword($passwordFromForm){
        try{
            $db = DB::getInstance();
            $query = $db->prepare('SELECT USERS_PASSWORD FROM users WHERE USERS_ID = :userID');
            $query->execute([
                "userID" => $this->getUSERSID()
            ]);
            $result =  $query->fetch();
            return password_verify($passwordFromForm, $result["USERS_PASSWORD"]);
        }
        catch(\Exception $ex){
            logs::PostALog('user._CheckUserPassword', 'error', $ex->getMessage());
        }
    }

    private function _AttributeNewTempTokenToUser(){
        // source : https://thisinterestsme.com/generating-random-token-php/
        try{
            $db = DB::getInstance();
            //Generate a random string.
            $token = openssl_random_pseudo_bytes(16);
            //Convert the binary data into hexadecimal representation.
            $token = bin2hex($token);

            if($token != ""){
                $query = $db->prepare("UPDATE users SET USERS_TEMPTOKEN =:tempToken, USERS_TEMPTOKENVALIDITY= SYSDATE()+ INTERVAL 1 DAY WHERE USERS_ID=:userId");
                $query->execute([
                    "tempToken"=> $token,
                    "userId"=> $this->getUSERSID()
                ]);
            }
            return $token;

        }
        catch(\Exception $ex){
            logs::PostALog('user._AttributeNewTempTokenToUser', 'error', $ex->getMessage());
        }
    }
}