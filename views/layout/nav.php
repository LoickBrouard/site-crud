<?php ob_start();?>
<nav>
    <div class="welcome">
        <p>Welcome <?= $_SESSION['USERNAME'] = $_SESSION['USERNAME']?? "mylady, sir"; ?> !</p>
    </div>
    <div class="title">
        <p> -<?= $_SESSION['PAGENAME'] = $_SESSION['PAGENAME']?? ""; ?>-</p>
    </div>
    <div class="navIcons">
        <a href="https://gitlab.com/LoickBrouard/site-crud" target="_blank"><button class="gitlabIcon">GitLab</button></a>
        <a href="/index.php"><button class="homeIcon">Home</button></a>
        <a href="/index.php?controller=Profile&action=Index"><button class="profileIcon">Profile</button></a>
        <?php
        if(isset($_SESSION["ROLE"]) && $_SESSION["ROLE"] == 'admin'){
            echo '<a href="/index.php?controller=Admin&action=Index"><button class="adminIcon">Admin</button></a>';
        }
        ?>
        <a href="/index.php?controller=Connection&action=LogOut" ><button class="logoutIcon">Logout</button></a>
    </div>
</nav>
<?php $nav = ob_get_clean() ?>