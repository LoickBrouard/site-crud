<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon"  type="image/ico" href="/public/favicon.ico"/>
    <link rel="stylesheet" href="/public/assets/css/site.css">
    <?= $css=$css??"" ?>
    <title>ExoCRUD</title>
</head>
<body>
<?= $nav = $nav??""?>
<?= $content=$content??"" ?>
</body>

<script src="/public/assets/js/site.js"></script>
<?= $script=$script??"" ?>
</html>