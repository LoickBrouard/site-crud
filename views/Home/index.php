<?php
ob_start();
$css = '<link rel="stylesheet" href="/public/assets/css/home.css">';
$script='<script src="/public/assets/js/home.js"></script>';
?>

<main>
    <div class="flex flexColumn">
        <h2>Great content</h2>
    </div>
    <div class="dataTable" id="playlist">
        <table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Author</th
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Lemon Tree</td>
                    <td>Fool's Garden</td>
                </tr>
                <tr>
                    <td>Sultans Of Swing</td>
                    <td>Dire Strait</td>
                </tr>
                <tr>
                    <td>Rappers Delight</td>
                    <td>Sugarhill Gang</td>
                </tr>
                <tr>
                    <td>King Kong</td>
                    <td>Thomas Fersen</td>
                </tr>
                <tr>
                    <td>Talk Too Much</td>
                    <td>Coin</td>
                </tr>
                <tr>
                    <td>Killing Me Softly With His Song</td>
                    <td>Roberta Flack</td>
                </tr>
                <tr>
                    <td>Baby Did A Bad Bad Thing</td>
                    <td>Chris Isaak</td>
                </tr>
                <tr>
                    <td>Le défilé</td>
                    <td>Boris Vian</td>
                </tr>
            </tbody>


        </table>
    </div>
</main>
<?php $content = ob_get_clean() ?>