<?php
$css = '<link rel="stylesheet" href="/public/assets/css/connection.css">';
$script = '<script src="/public/assets/js/connection.js"></script>';
ob_start();

?>
<div class="buttonContainer">
    <input type="button" id="connectionButton" value="Connection">
    <input type="button" id="registerButton" value="Register">
</div>
<div >
    <p class="alert"><?= $alert = $alert?? ""?></p>
    <p class="message"><?= $message = $message?? ""?></p>
    <?php
        unset($_SESSION["MESSAGE"]);
        unset($_SESSION["ERROR"]);
    ?>
</div>
<form method="POST" id="connectionForm" action="/index.php?controller=Connection&action=Connection" >
    <div class="grid-container fiveElementsGrid FormContainerMedium">
        <div class="grid-title">
            <h1>Connexion</h1>
        </div>
        <label for="idForm" class="grid-label">LOGIN</label>
        <input type="text" name="idForm" class="grid-input" placeholder="Login" required>
        <label for="pwdForm" class="grid-label">Pwd</label>
        <input type="password" name="pwdForm" class="grid-input" placeholder="Password" required>
        <input type="submit" class="grid-input" value="ENTER">

    </div>
</form>

<form method="POST" id="registerForm" action="/index.php?controller=Connection&action=Register" >
    <div class="grid-container nineElementsGrid FormContainerTall tallForm">
        <div class="grid-title">
            <h1>Inscription</h1>

        </div>
        <label for="loginForm" class="grid-label">LOGIN</label>
        <input type="text" name="loginForm" class="grid-input" placeholder="Login" value="<?= $_SESSION["loginForm"]= $_SESSION["loginForm"]?? "" ?>" required>
        <label for="nameForm" class="grid-label">Name</label>
        <input type="text" name="nameForm" class="grid-input" placeholder="Name" value="<?= $_SESSION["nameForm"]= $_SESSION["nameForm"]?? "" ?>" required>
        <label for="phoneForm" class="grid-label">Phone</label>
        <input type="tel" name="phoneForm" class="grid-input" placeholder="01 23 45 67 89" value="<?= $_SESSION["phoneForm"]= $_SESSION["phoneForm"]?? "" ?>">
        <label for="addressForm" class="grid-label">Address</label>
        <input type="text" name="addressForm" class="grid-input" placeholder="30, Chemin de Traverse" value="<?= $_SESSION["addressForm"]= $_SESSION["addressForm"]?? "" ?>">
        <label for="zipForm" class="grid-label">Zip Code</label>
        <input type="text" name="zipForm" class="grid-input" placeholder="82000" value="<?= $_SESSION["zipForm"]= $_SESSION["zipForm"]?? "" ?>">
        <label for="cityForm" class="grid-label">City</label>
        <input type="text" name="cityForm" class="grid-input" placeholder="Montauban" value="<?= $_SESSION["cityForm"]= $_SESSION["cityForm"]?? "" ?>">

        <label for="pwdForm" class="grid-label">Pwd</label>
        <input type="password" name="pwdForm" class="grid-input" placeholder="Password" required>
        <label for="pwdCheckForm" class="grid-label">Pwd Check</label>
        <input type="password" name="pwdCheckForm" class="grid-input" placeholder="Password" required>
        <input type="submit" class="grid-input" value="ENTER">
    </div>
</form>

<?php $content = ob_get_clean() ?>