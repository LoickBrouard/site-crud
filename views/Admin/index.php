<?php
ob_start();
$css = '<link rel="stylesheet" href="/public/assets/css/admin.css">';
$script='<script src="/public/assets/js/admin.js"></script>';
?>

<main>
    <div>
        <h2>USERS MANAGEMENT</h2>
        <table>
            <thead>
            <tr>
                <th>User</th>
                <th>Admin</th>
                <th>Update</th>
                <th>Remove</th>
            </tr>
            </thead>
            <tbody id="usersTable">
            <?php
            $usersInfo = $usersInfo ?? "";
            if ($usersInfo != "") {
                foreach ($usersInfo as $user) {
                    if ($user->getUSERSLOGIN() != 'admin') {
                        $checked = ($user->getUSERSROLE() == 'admin') ? 'checked' : '';
                        echo '<tr>';
                        echo '<td>' . $user->getUSERSLOGIN() . '</td>';
                        echo '<td class="iconData"><label class="switch"><input type="checkbox"' . $checked . '><span class="slider"></span></label></td>';
                        echo '<td class="iconData"><a id="updateIcon" class="updateIcon icon" href="#"></a></td>';
                        echo '<td class="iconData"><a href="#" id="removeIcon" class="removeIcon icon"></a></td>';
                        echo '</tr>';

                    }
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    <div>
        <h2>LOGS</h2>
        <table id="logTable">
            <thead>
            <tr>
                <th>Date</th>
                <th>Function</th>
                <th>Level</th>
                <th>Message</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $logs = $logs ?? "";
            if ($logs != "") {
                foreach ($logs as $log) {
                    echo '<tr>';
                    echo '<td>' . $log->getLOGDATE() . '</td>';
                    echo '<td>' . $log->getLOGFUNCTION() . '</td>';
                    echo '<td>' . $log->getLOGLEVEL() . '</td>';
                    echo '<td>' . $log->getLOGMESSAGE() . '</td>';
                    echo '</tr>';
                }
            }
            ?>
            </tbody>
        </table>
    </div>
    </main>

<?php $content = ob_get_clean() ?>