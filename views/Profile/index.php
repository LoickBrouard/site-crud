<?php
ob_start();
$css = '<link rel="stylesheet" href="/public/assets/css/profile.css">';
$script='<script src="/public/assets/js/profile.js"></script>';
?>
<main>
    <div >
        <p class="alert"><?= $alert = $alert?? ""?></p>
        <p class="message"><?= $message = $message?? ""?></p>
        <?php
            $alert = '';
            $message = '';
            ?>
    </div>
    <form action="/index.php?controller=Profile&action=UpdateUserInfoWithUserAccount" method="post">
        <h2>PERSONNAL INFORMATIONS</h2>
        <div class="category">
           <div class="formRow">
               <label for="nameForm" >Name</label>
               <input type="text" name="nameForm" value="<?= $nameForm = $user['nameForm']?? "" ?>" required>
           </div>
            <div class="formRow">
                <label for="phoneForm" >Phone</label>
                <input type="tel" name="phoneForm" value="<?= $phoneForm= $user['phoneForm']?? "" ?>">
            </div>
            <div class="formRow">
                <label for="addressForm" >Address</label>
                <input type="text" name="addressForm" value="<?= $addressForm= $user['addressForm']?? "" ?>">
            </div>
            <div class="formRow">
                <label for="zipForm" >Zip Code</label>
                <input type="text" name="zipForm" value="<?= $zipForm= $user['zipForm']?? "" ?>">
            </div>
            <div class="formRow">
                <label for="cityForm" >City</label>
                <input type="text" name="cityForm" value="<?= $cityForm= $user['cityForm']?? "" ?>">
            </div>
        </div>
        <h2>CHANGE PASSWORD</h2>
        <div class="category">
            <div class="formRow">
                <label for="pwdFormCurrent">Current Pwd</label>
                <input type="password" name="pwdFormCurrent">
            </div>
            <div class="formRow">
                <label for="pwdForm">New Pwd</label>
                <input type="password" name="pwdForm">
            </div>
            <div class="formRow">
                <label for="pwdCheckForm">New Pwd Check</label>
                <input type="password" name="pwdCheckForm">
            </div>
        </div>
        <div class="submit">
        <input type="submit" value="MODIFY">
        </div>
        </div>
    </form>
</main>

<?php $content = ob_get_clean() ?>