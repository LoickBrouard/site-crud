
let connectionForm = document.querySelector("#connectionForm");
let registerForm = document.querySelector("#registerForm");
let connectionButton = document.querySelector("#connectionButton");
let registerButton = document.querySelector("#registerButton");
connectionButton.classList.add('focused');

registerForm.classList.add('displayNone');

connectionButton.addEventListener('click', () =>{
    registerForm.classList.add('displayNone')
    connectionForm.classList.remove('displayNone');
    connectionButton.classList.add('focused');
    registerButton.classList.remove('focused');
})
registerButton.addEventListener('click', () =>{
    registerForm.classList.remove('displayNone')
    connectionForm.classList.add('displayNone');
    connectionButton.classList.remove('focused');
    registerButton.classList.add('focused');
})
