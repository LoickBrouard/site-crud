let table = document.querySelector('#usersTable');

table.addEventListener('click', (e)=>{
    let user = '';
    let role = '';

    if(e.toElement.classList.contains('updateIcon')){
        user = e.target.parentElement.parentElement.children[0].innerHTML;
        role = (e.target.parentElement.parentElement.children[1].children[0].children[0].checked)? 'admin': 'user';
        updateUser(user, role);
    }
    else if(e.toElement.classList.contains('removeIcon')){
        user = e.target.parentElement.parentElement.firstChild.innerHTML;
        removeUser(user);
    }
});

function removeUser(user){
    let url = '/index.php?controller=Admin&action=RemoveOneUserFromLogin';
    let fd = new FormData();
    fd.append('login', user);

    PostToController(url, fd);
}

function updateUser(user, role){
    let url = '/index.php?controller=Admin&action=UpdateOneUserFromLogin';
    let fd = new FormData();
    fd.append('login', user);
    fd.append('role', role);

    PostToController(url, fd);
}

function PostToController(url, formdata){
    const options = {
        method: 'POST',
        body: formdata,
    }
    fetch(url, options)
        .then(response => response.json())
        .then((res)=>{
            if(res.error == ""){
                window.location.reload();
            }
            else{
                alert(res.error);
            }
        })
        .catch( (err)=> {
            alert('Server unreachable, we are checking if there is no fire in the server room')
            console.log(err);
        } );
}