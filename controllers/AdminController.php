<?php
namespace controllers;

use models\DB;
use models\logs;
use models\user;

class AdminController extends Controller {
    function index(){
        $_SESSION['PAGENAME'] = 'ADMIN';
        if($_SESSION["ROLE"]== 'admin'){
            $usersInfo = new user();
            $userList = $usersInfo->GetAdminInfoOfAllUsers();
            $logsObject = new logs();
            $logslist = $logsObject->GetAllLogs();
            $this->render('index', $nav=true, $data=['usersInfo'=> $userList, 'logs'=> $logslist]);
        }
        else
            $this->renderUnauthorized();
    }

    function RemoveOneUserFromLogin(){
        $response = [
            'error'=> '',
            'result' => false
        ];
        try {
            if (isset($_POST['login']) && $_POST['login'] != 'admin') {
                $user = new user();
                $user->setUSERSLOGIN($_POST['login']);
                $user->RemoveUser();
            }
            echo json_encode($response);
        } catch (\Exception $e) {
            logs::PostALog('AdminController.RemoveOneUserFromLogin', 'error', $e->getMessage());
        }
    }
    function UpdateOneUserFromLogin(){
        $response = [
            'error'=> '',
            'result' => false
        ];
        try {
            if (isset($_POST['login']) && isset($_POST['role']) && $_POST['login'] != 'admin') {
                $user = new user();
                $user->setUSERSLOGIN($_POST['login']);
                $user->setUSERSROLE($_POST['role']);
                $user->UpdateUserInfoFromAdminAccount();
            }
            echo json_encode($response);
        }
        catch (\Exception $e) {
            logs::PostALog('AdminController.UpdateOneUserFromLogin', 'error', $e->getMessage());
        }
    }
}