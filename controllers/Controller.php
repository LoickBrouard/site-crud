<?php
namespace controllers;

Abstract Class Controller{
    /**
     * Display a view
     *
     * @param string $viewName
     * @param array $data
     * @return void
     */
    public function __construct(){
        if(!isset($_SESSION)){
            session_start();
        }
    }

    public function render(string $viewName, $includeNav=false, array $data = []){
        $viewFolder = str_replace('controllers\\', '',get_class($this));
        $viewFolder = str_replace('Controller','',$viewFolder);
        extract($data);

        // Create and require the view
        $includeNav? require_once (ROOT.'views/layout/nav.php') : "";

        require_once(ROOT.'views/'.$viewFolder.'/'.$viewName.'.php');
        require_once (ROOT.'views/layout/default.php');
    }

    public function renderUnauthorized(){
        require_once (ROOT.'views/layout/404.php');
        require_once (ROOT.'views/layout/default.php');
    }

    public function InputTreatment($input)
    {
        $input = trim($input);
        $input = stripcslashes($input);
        return htmlspecialchars($input);
    }
}
