<?php
namespace controllers;

use models\DB;
use models\user;
use models\logs;

class ProfileController extends Controller {
    function index($message=null, $alert=null){
        $_SESSION['PAGENAME'] = 'PROFILE';
        if(isset($_SESSION['TEMPTOKEN']))
            $user = new user();
            $user->setUSERSTEMPTOKEN($_SESSION['TEMPTOKEN']);
            if($user->IsTokenStillValid()){
                $userInfo = $user->GetSafeUserInfoForOneUserFromTempToken();
                $formInfo = [
                    'nameForm' => $userInfo->getUSERSNAME(),
                    'phoneForm' => $userInfo->getUSERSPHONE(),
                    'addressForm' => $userInfo->getUSERSADDRESS(),
                    'zipForm' => $userInfo->getUSERSZIP(),
                    'cityForm' => $userInfo->getUSERSCITY()
                ];
                $this->render('index', $includeNav=true, $data=['userName'=> $userInfo->getUSERSNAME(), 'user' => $formInfo, 'message'=>$message, 'alert'=>$alert]);
            }
        }

    function UpdateUserInfoWithUserAccount(){
        $alert = '';
        $message='';
        try {
            unset($_SESSION['ERROR']);

            $newUserInfo = new user();
            $newUserInfo->setUSERSTEMPTOKEN($_SESSION['TEMPTOKEN']);
            $newUserInfo = $newUserInfo->GetAllInfoForOneUserFromTempToken();

            $newUserInfo->setUSERSNAME($this->InputTreatment($_POST["nameForm"]));
            $newUserInfo->setUSERSPHONE($this->InputTreatment($_POST["phoneForm"]));
            $newUserInfo->setUSERSADDRESS($this->InputTreatment($_POST["addressForm"]));
            $newUserInfo->setUSERSZIP($this->InputTreatment($_POST["zipForm"]));
            $newUserInfo->setUSERSCITY($this->InputTreatment($_POST["cityForm"]));
            if ($newUserInfo->UpdateAllUserInfoFromUserAccount()) {
                $message = 'Profile updated';
            }
            else {
                throw new \Exception("Eror while updating your profile... unlucky you");
            }

            if(($_POST['pwdForm'] !="" || $_POST['pwdCheckForm'] !="" || $_POST['pwdFormCurrent'] !="")){
                if($_POST['pwdForm'] !="" && $_POST['pwdCheckForm'] !="" && $_POST['pwdFormCurrent'] !=""){
                    $passwordCurrent = $_POST['pwdFormCurrent'];
                    $passwordNew = $_POST['pwdForm'];
                    $passwordNewCheck = $_POST['pwdCheckForm'];
                    $newUserInfo->UpdateUserPassword($passwordCurrent, $passwordNew, $passwordNewCheck);
                }
                else
                    throw new \Exception('One of the field is missing to change your password');
            }
        }
        catch (\Exception $e) {
            $message ='';
            logs::PostALog('ProfileController.UpdateUserInfoWithUserAccount', 'error', $e->getMessage());
            $alert = 'Something went wrong...'.$e->getMessage();
        }
        finally{
            $formInfo = [
                'nameForm' => $newUserInfo->getUSERSNAME(),
                'phoneForm' => $newUserInfo->getUSERSPHONE(),
                'addressForm' => $newUserInfo->getUSERSADDRESS(),
                'zipForm' => $newUserInfo->getUSERSZIP(),
                'cityForm' => $newUserInfo->getUSERSCITY()
            ];
            $this->render('index', $includeNav = true, $data = ['user' => $formInfo, 'message' => $message, 'alert'=> $alert]);
        }
    }
}