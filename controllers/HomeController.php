<?php
namespace controllers;

use models\DB;
use models\user;

class HomeController extends Controller {

    function index(){
        try{
            $_SESSION['PAGENAME'] = 'HOME';
            if(isset($_SESSION['TEMPTOKEN'])){
                $user = new user();
                $user->setUSERSTEMPTOKEN($_SESSION['TEMPTOKEN']);
                if($user->IsTokenStillValid()){
                    $userInfo = $user->GetAllInfoForOneUserFromTempToken();
                    $_SESSION['USERNAME'] = $userInfo->getUSERSNAME();
                    $this->render('index', $includeNav=true);
                    exit();
                }
            }
        }
        catch(\Exception $e){
            $_SESSION["ERROR"] = "An error occured while getting your information";
        }
        header('location:index.php?controller=Connection');
        exit();
    }
}

