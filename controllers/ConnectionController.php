<?php
namespace controllers;
use models\user;
use models\DB;
use models\logs;



class ConnectionController extends Controller {
    function index(){
        $alert =  $_SESSION['ERROR']??null;
        $message =  $_SESSION['MESSAGE']??null;
        $this->render('index',$includeNav=false, ['alert' => $alert, 'message' => $message]);
    }

    function Connection(){
        try{
            unset($_SESSION['ERROR']);
            $result = $this->CheckConnectionInput($_POST);
            if($result['success']){
                $_SESSION['TEMPTOKEN'] = $result['temptoken'];
                $_SESSION['ROLE'] = $result['role'];
                $_SESSION['USERNAME'] = $result['username'];
                header('location:index.php?controller=Home');
                exit();
            }
            else
                $_SESSION['ERROR'] = $result['error'];

        }
        catch(\Exception $e){
            logs::PostALog('ConnectionController.Connection', 'error', $e->getMessage());
            $_SESSION['ERROR'] = $e->getMessage();
        }

        header('location:index.php?controller=Connection');
        exit();
    }

    function Register(){
        try{
            unset($_SESSION['ERROR']);
            $user = new user();
            $user->setUSERSLOGIN($this->InputTreatment($_POST["loginForm"]));
            $user->setUSERSPASSWORD($this->InputTreatment($_POST["pwdForm"]),$this->InputTreatment($_POST["pwdCheckForm"]));
            $user->setUSERSNAME($this->InputTreatment($_POST["nameForm"]));
            $user->setUSERSPHONE($this->InputTreatment($_POST["phoneForm"]));
            $user->setUSERSADDRESS($this->InputTreatment($_POST["addressForm"]));
            $user->setUSERSZIP($this->InputTreatment($_POST["zipForm"]));
            $user->setUSERSCITY($this->InputTreatment($_POST["cityForm"]));

            if(!user::IsThisUserLoginAvailable($user->getUSERSLOGIN())){
                throw new \Exception("This login is already taken, not fast enough young padawan...");
            }

            if($user->RegisterOneUser()){
                session_unset();
                $_SESSION["MESSAGE"] = "Registration successful, please come in :)";
                header('location:index.php?controller=Connection');

                exit;
            }
            else
                throw new \Exception("A problem occured while registration, please blame the programmer");
        }
        catch(\Exception $e){
            logs::PostALog('ConnectionController.Register', 'error', $e->getMessage());
            $_SESSION['ERROR'] = $e->getMessage();
            $_SESSION['loginForm'] = $_POST["loginForm"];
            $_SESSION['nameForm'] = $_POST["nameForm"];
            $_SESSION['phoneForm'] = $_POST["phoneForm"];
            $_SESSION['addressForm'] = $_POST["addressForm"];
            $_SESSION['zipForm'] = $_POST["zipForm"];
            $_SESSION['cityForm'] = $_POST["cityForm"];
            header('location:index.php?controller=Connection');
            exit();
        }
    }

    function LogOut(){
        session_unset();
        header('location:index.php?controller=Connection');
        exit();
    }

    function CheckConnectionInput($input){
        /*
         *  return array([
         * 'success' (bool)
         * 'temptoken' (string)
         * 'role' (string)
         * 'error' string
         * ]);
         * */
        try{
            if(isset($input)){
                $login = $input["idForm"];
                $psswd = $input["pwdForm"];
                if($login != "" && $psswd != ""){
                    $login = $this->InputTreatment($login);
                    $psswd = $this->InputTreatment($psswd);
                    $user = new user();
                    return $user->CheckUserLoginAndPassword($login, $psswd);
                }
            }
            else
                throw new \Exception('user not found');
        }
        catch(\Exception $e){
            logs::PostALog('ConnectionController.CheckConnectionInput', 'error', $e->getMessage());
            throw new \Exception('Something went wrong... go easy with customer service please');
        }
    }
}





