<?php

// Redefining root path
define('ROOT', str_replace('index.php','',$_SERVER['SCRIPT_FILENAME']));

// Class autoloader
function ClassLoader($CLASS){
    $DS = DIRECTORY_SEPARATOR;                           // get the current separator format using by the OS
    $CLASSPATH = str_replace("\\", $DS,$CLASS);   // replace the path format by the current separator format using by the OS

    $CLASSPATH = ROOT.$CLASSPATH;                       // Recreate the path to the class
    $FILE = $CLASSPATH.".php";                          // The class and the file share the same name

    if(is_readable($FILE))                              // If the file exists it is required
            require_once $FILE;
}

spl_autoload_register("ClassLoader");

// Récupération des infos de l'url
// url de type /? controller=ControllerValue & action=ControllerMethode & param=MethodeArgument

$CONTROLLER = (isset($_GET["controller"])) ? $_GET["controller"] : "";
$ACTION = (isset($_GET["action"])) ? $_GET["action"] : "";
$PARAM = (isset($_GET["param"])) ? $_GET["param"] : "";

if($CONTROLLER != ""){
    $CLASS = "controllers\\".$CONTROLLER."Controller";
    if(class_exists($CLASS)){
        $CONTROLLER = new $CLASS;
        if(method_exists($CLASS, $ACTION)){
            echo $CONTROLLER->$ACTION($PARAM);
        }
        else{
            echo $CONTROLLER->index();
        }
    }
    else{
        // On envoie le code réponse 404
        http_response_code(404);
        echo "La page recherchée n'existe pas";
    }
}
else{
    $CONTROLLER = new controllers\HomeController();
    echo $CONTROLLER->index();
}

